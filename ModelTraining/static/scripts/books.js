$(document).ready(function() {
    (function() {
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=minecraft",
            datatype: "json",
            success: function(data) {
                populateBook(data);
            }
        });
    })();
});

function search(data) {
    var query = $("input").val();
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + query,
        datatype: JSON,
        success: function(find) {
            $("#book")
                .find("tr:gt(0)")
                .remove();
            populateBook(find);
        }
    });
}

function getQuery() {
    var query = $("input[name='pencarian']").val();
    search(query);
}

function populateBook(find) {
    for (var i = 0; i < find.items.length; i++) {
        var title = find.items[i].volumeInfo.title;
        var author = find.items[i].volumeInfo.authors;
        var publisher = find.items[i].volumeInfo.publisher;
        if (author == undefined) author = "No author given";
        if (publisher == undefined) publisher = "No publisher given";
        $("#book").append(
            "<tr>" +
            '<td class=""><img src="' +
            find.items[i].volumeInfo.imageLinks.smallThumbnail +
            '"></td>' +
            '<td class="">' +
            title +
            "</td>" +
            '<td class="">' +
            author +
            "</td>" +
            '<td class="">' +
            publisher +
            "</td>" +
            "</tr>"
        );
    }
}
