from django.db import models
import datetime
import pytz
import Story5.settings

class CForm(models.Model):
    id = models.AutoField(primary_key = True)
    aktivitas = models.CharField(max_length = 100)
    tanggal = models.DateField(null = True)
    jam = models.TimeField(null = True)
    tempat = models.CharField(max_length =200)
    kategori = models.CharField(max_length =200)
    
# Create your models here.
