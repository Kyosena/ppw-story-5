from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
class Lab9UnitTests(TestCase):
    def test_url_isExists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
        invalid_response = Client().get('/this_should_not_exist')
        self.assertEqual(invalid_response.status_code, 404)
    
    def test_landing_using_template(self):
        response = Client().get('/regis')
        self.assertTemplateUsed(response, 'indexauth.html')

    def test_story7_using_index_function(self):
        Function = resolve('/regis')
        self.assertEqual(Function.func, register)