from django import forms
from .models import CForm
from django import forms
from ModelTraining import models
from django.utils import timezone
class ContactForm(forms.Form):
    event_attrs = { 
        "type":"text",
        "placeholder":"Event Name",
        "name":"event",
        "value":"",
    }
    calendar_attrs = {
        "type":"date",
        "name":"tanggal",
        "value":"",
    }

    time_attrs = {
        "type":"time",
        "name":"jam",
        "value":"",
    }
    aktivitas = forms.CharField(label =" Masukan Aktivitas Anda", max_length = 100)
    tanggal = forms.DateField(initial=timezone.now, required = True, widget=forms.DateInput(attrs=calendar_attrs))
    jam = forms.TimeField(initial=timezone.now, required=True,widget=forms.TimeInput(attrs=time_attrs))
    tempat = forms.CharField(label =" Masukan Tempat Aktivitas Anda", max_length = 100)
    kategori = forms.CharField(label =" Masukan Kategori Aktivitas Anda", max_length = 100)