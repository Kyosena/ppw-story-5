from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import views as auth_views
# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            messages.success(request, f"Account Successfully created for {username}.")
            return redirect("/login/")
    else:
        form = UserCreationForm()
    return render(request,'indexauth.html',{'form' : form})
    
def login(request):
    return render(request,"loginn.html")
def logout(request):
    return render(request,"logout.html")