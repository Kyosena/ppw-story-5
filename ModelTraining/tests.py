from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *

class Lab7UnitTests(TestCase):
    def test_url_isExists(self):
        response = Client().get('/Signup/')
        self.assertEqual(response.status_code, 200)
        invalid_response = Client().get('/this_should_not_exist')
        self.assertEqual(invalid_response.status_code, 404)
    
    def test_landing_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story7_using_index_function(self):
        Function = resolve('/')
        self.assertEqual(Function.func, Home)
    def test_story10_blogpost_isExists(self):
        response = Client().get('/Blogpost/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "blog.html")
    def test_landing_blog_function(self):
        Function = resolve('/Blogpost/')
        self.assertEqual(Function.func, Blog)