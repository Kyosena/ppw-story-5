from django.apps import AppConfig


class ModeltrainingConfig(AppConfig):
    name = 'ModelTraining'
